<?php

require_once __DIR__ . '/vendor/autoload.php';

if ($argc !== 3) {
    echo 'Expected 2 arguments: src dir, and test dir. e.g. testifier src/ tests/';
    exit;
}

$testifier = new Testify\Testifier();
$testifier->testify($argv[1], $argv[2]);