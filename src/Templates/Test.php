<?php
declare(strict_types=1);

namespace Testify\Templates;

class Test
{
    public function make(string $className)
    {
        return "<?php
declare(strict_types=1);

namespace App\Tests\unit;

use Codeception\Test\Unit;

class $className extends Unit
{
}";
    }
}
