<?php
declare(strict_types=1);

namespace Testify;

use Exception;
use Testify\Templates\Test;

class Testifier
{
    private string $testDir = '';

    public function testify(string $servicesDir, string $testDir): void
    {
        $this->testDir = $testDir;
        $this->makeTests($this->findServices($servicesDir), $this->findTests($testDir));
    }

    private function findServices(string $servicesDir): array
    {
        if (!is_dir($servicesDir)) {
            throw new Exception("$servicesDir is not a directory");
        }

        $services = [];
        foreach (scandir($servicesDir) as $item) {
            if (substr($item, -4) === '.php') {
                $services[] = $item;
            }
        }

        return $services;
    }

    private function findTests(string $testDir): array
    {
        if (!is_dir($testDir)) {
            throw new Exception("$testDir is not a directory");
        }

        $tests = [];
        foreach (scandir($testDir) as $item) {
            if (substr($item, -4) === '.php') {
                $tests[] = str_replace('Test.php', '.php', $item);
            }
        }

        return $tests;
    }

    private function makeTests(array $services, array $tests): void
    {
        $missing = array_map(
            fn(string $file) => str_replace('.php', 'Test.php', $file),
            array_diff($services, $tests),
        );

        if (count($missing) === 0) {
            echo 'All tests accounted for, exiting';
            return;
        }

        $prompt = count($missing) > 5 ? count($missing) . ' new files' : implode(', ', $missing);

        $consent = readline("This will add $prompt, continue? (Y/n)");

        if (in_array($consent, ['n', 'N'])) {
            return;
        }

        $template = new Test();

        foreach ($missing as $file) {
            $className = str_replace('.php', '', $file);
            $dir = sprintf('%s/%s', $this->testDir, $file);
            file_put_contents($dir, $template->make($className));
        }
    }
}
